\babel@toc {nil}{}
\contentsline {chapter}{\numberline {1}Fazi Skupovi}{1}{chapter.2}% 
\contentsline {section}{\numberline {1.1}Istorija fazi skupova}{1}{section.3}% 
\contentsline {section}{\numberline {1.2}Osnove fazi skupova}{2}{section.4}% 
\contentsline {section}{\numberline {1.3} Vrste karakteristi\IeC {\v c}nih funkcija }{3}{section.15}% 
\contentsline {subsection}{\numberline {1.3.1}Trougaona karakteristi\IeC {\v c}na funkcija}{3}{subsection.16}% 
\contentsline {subsection}{\numberline {1.3.2}Trapezoidna karakteristi\IeC {\v c}na funkcija}{3}{subsection.18}% 
\contentsline {subsection}{\numberline {1.3.3}Gausova karakteristi\IeC {\v c}na funkcija}{4}{subsection.20}% 
\contentsline {chapter}{\numberline {2}Fazi logika}{5}{chapter.22}% 
\contentsline {section}{\numberline {2.1}Osobine fazi logike}{5}{section.23}% 
\contentsline {section}{\numberline {2.2}Vi\IeC {\v s}evrednosna logika}{6}{section.28}% 
